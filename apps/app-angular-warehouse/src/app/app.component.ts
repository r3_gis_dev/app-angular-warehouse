import { Component } from '@angular/core';

@Component({
  selector: 'r3gis-test-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {}

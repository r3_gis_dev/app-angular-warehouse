import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { DataAccessService } from './data-access.service';

@NgModule({
  imports: [CommonModule],
  declarations: [],
  providers: [DataAccessService],
  exports: [],
})
export class DataAccessModule {}

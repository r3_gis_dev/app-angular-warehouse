import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import { delay, map, tap } from 'rxjs/operators';

export enum ProductCategory {
  PersonalCare = 'PersonalCare',
  DogFood = 'DogFood',
  Falcon5Component = 'Falcon5Component',
}

export interface Warehouse {
  name: string;
  code: number;
}

export interface Product {
  name: string;
  description: string;
  exporationDate?: Date;
  code: number;
  category: ProductCategory;
  warehouse: number;
}

@Injectable()
export class DataAccessService {
  private stores: Array<Warehouse> = [];
  private products: Array<Product> = [];

  constructor() {
    this.stores = [
      {
        name: 'R3Gis Server Room',
        code: 424242,
      },
      {
        name: '"Lost" items',
        code: 4815162342,
      },
      {
        name: 'Firespray 31',
        code: 3010301,
      },
    ];

    this.products = [
      {
        name: 'Dragon 2 crew capsule',
        description: 'A dragon capsule with monitors and seats',
        code: 2021,
        category: ProductCategory.Falcon5Component,
        warehouse: 424242,
      },
      {
        name: 'Han Solo',
        description: 'Han Solo body in carbonite',
        code: 123413,
        category: ProductCategory.PersonalCare,
        warehouse: 3010301,
      },
    ];
  }

  /**
   * List all the Warehouses
   */
  getWarehouses() {
    return of(this.stores).pipe(delay(500));
  }

  /**
   * Get all the products, can be filtered by warehouse
   */
  getProducts(filter?: { warehouse: number }) {
    return of(this.products).pipe(
      delay(500),
      map((products) => {
        if (filter) {
          return products.filter((prod) => prod.warehouse === filter.warehouse);
        }
        return products;
      })
    );
  }

  /**
   * Create one product, fail 40% of the times
   * @param product Il prodotto da creare
   */
  createProduct(product: Product) {
    return of(Math.random() > 0.4).pipe(
      delay(500),
      tap((result) => (result ? this.products.push(product) : null))
    );
  }

  /**
   * Delete one product, fail 40% of the times
   * @param id The id of the product to delete
   */
  deleteProduct(codeToDel: number) {
    return of(Math.random() > 0.4).pipe(
      delay(500),
      tap((result) => {
        if (result) {
          this.products = this.products.filter(
            ({ code }) => code !== codeToDel
          );
        }
      })
    );
  }
}

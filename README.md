# Warehouses

## Specifications

This is an example project for an amministration portal for a warehouse company.
The main and only actor that will interact with this software will be a administrator.

This administrator can:

- See all the warehouses
- See all the items into one warehouse
- Add items to a warehouse
- Remove items from a warehouse

You should develop all this features.

## Development

Rememeber to run `yarn install` after cloneing this repo.

This project uses [nx](https://nx.dev/). You can use the nx cli throught yarn, for example to start the app just run `yarn nx serve app-angular-warehouse`. You must use as a data access library `@r3gis-test/data-access-lib` that is located inside this workspace in `lib/data-access-lib`. Use this lib as a references for the types and methods of access.

## Assignment

You should clone this repo and make the development on a different branch than master. When you are ready just create a pull request to master to submit the project for review.

We are not going to review the estetic of the application itself, so don't waste too much time on css, just make the interface usable.
